import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  getUsers(): Array<{}>  {
    return [{
          name: 'Arnaldo',
          surname: 'Ceballos'
      }, {
          name: 'Facundo',
          surname: 'Barboza'
      }, {
        name: 'Lucas',
        surname: 'Domene'
    }
    ];
  }
}
