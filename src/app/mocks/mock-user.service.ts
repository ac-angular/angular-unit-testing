import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MockUserService {

  constructor() { }

  getUsers(): Array<{}> {
    return [
        {
            name: 'Raúl',
            surname: 'Degni'
        }
    ];
  }
}
