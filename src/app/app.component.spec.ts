import { TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { DebugElement } from '@angular/core';

describe('AppComponent', () => {
  let comp: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ]
    }).compileComponents().then(() => { // promesa
      fixture = TestBed.createComponent(AppComponent); // crea el componente
      comp = fixture.componentInstance; // fixture permite crear una instancia del componente
    });
  });

  it('should create the app', () => { // Chequeo de instancia
    expect(comp).toBeTruthy();
  });

  it(`should have as title 'angular-unit-testing'`, () => { // Chequeo de propiedad
    expect(comp.title).toEqual('Angular Unit Testing');
  });

  it('should render title', () => { // Chequeo de renderizado
    fixture.detectChanges(); //aplica los cambios del componente al HTML
    const compiled = fixture.debugElement.nativeElement; // se obtiene el elemento renderizado
    expect(compiled.querySelector('.content span').textContent).toContain('Angular Unit Testing app se está ejecutando!');
  });
});
