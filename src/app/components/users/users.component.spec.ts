import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { MockUserService } from 'src/app/mocks/mock-user.service';
import { UserService } from 'src/app/services/user.service';

import { UsersComponent } from './users.component';

describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsersComponent ],
      providers: [ // hay que agregar los providers al modulo de testing
        { provide: UserService, useClass: MockUserService } // cuando el servicio UserService es inyectado deberia usar MockUserService
      ]
    })
    .compileComponents().then(() => { // promesa
      fixture = TestBed.createComponent(UsersComponent);
      component = fixture.componentInstance;
    });;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have one user', () => {
    expect(component.users.length).toEqual(1);
  });

  it('HTML should render one user', () => {
    fixture.detectChanges();
    const el = fixture.debugElement.query(By.css('p')).nativeElement;
    expect(el.textContent).toContain('Raúl');
  })
});
