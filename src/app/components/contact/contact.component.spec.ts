import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { ContactComponent } from './contact.component';

describe('ContactComponent', () => { // el bloque de testing se declara con el mismo nombre que el componente
  let component: ContactComponent;
  let fixture: ComponentFixture<ContactComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContactComponent ],
      imports: [
        FormsModule,
        ReactiveFormsModule
      ]
    })
    .compileComponents().then(() => { // promesa
      fixture = TestBed.createComponent(ContactComponent); // Estos se denomina test scope objects y se declaran aqui debido a que se usan en diferentes tests
      component = fixture.componentInstance;
      de = fixture.debugElement.query(By.css('form'));
      el = de.nativeElement;
    });;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create ContactComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should set submitted to true', () => {
    component.onSubmit();
    expect(component.submitted).toBeTruthy();
  });

  it('should call the onSubmit method', () => {
    fixture.detectChanges();
    spyOn(component, 'onSubmit'); // se crea un jasmine spy sobre la funcion onSubmit del componente
    el = fixture.debugElement.query(By.css('button')).nativeElement; // Obtiene el boton submit del DOM
    el.click(); // dispara el evento click
    expect(component.onSubmit).toHaveBeenCalledTimes(0); // esperamos que la funcion espiada no sea ejecutada ya que el boton esta deshabilitado debido a que el formluario es invalido
  });

  it('form should be invalid', () => {
    component.contactForm.controls['name'].setValue('');
    component.contactForm.controls['email'].setValue('');
    component.contactForm.controls['text'].setValue('');
    expect(component.contactForm.valid).toBeFalsy();
  });

  it('form should be valid', () => {
    component.contactForm.controls['name'].setValue('Arnaldo');
    component.contactForm.controls['email'].setValue('arnaldoceballos@gmail.com');
    component.contactForm.controls['text'].setValue('Hola!!');
    expect(component.contactForm.valid).toBeTruthy();
  });
});
