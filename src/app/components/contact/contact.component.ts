import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  contactForm: any;
  submitted = false;

  constructor() {
    this.createForm();
  }

  ngOnInit(): void {
  }

  createForm(): void {
    this.contactForm = new FormGroup({
      'name': new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ]),
      'email': new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      'text': new FormControl('', [
        Validators.required
      ])
    });
  }

  onSubmit(): void {
    this.submitted = true;
  }
}
